<?php

class Product
{
    public $name = "Een bepaald spel";
    public $description;
    public $price;
    public $category = "dranken"; // Added property for category with default value "dranken"
    public $rating = 3; // Added property for rating with default value 3
    public $currency = "€"; // Added currency property

    public function __construct($name, $description, $category = "dranken", $rating = 3)
    {
        $this->name = strtolower($name);
        $this->description = ucfirst($description);
        $this->category = $category;
        $this->rating = $rating;
    }

    public function formatPrice()
    {
        return number_format($this->price, 0);
    }

    public function getProduct()
    {
        return "Het product " . $this->name . " kost " . $this->currency . $this->price;
    }
}

$game1 = new Product("FIFA 2023", "Voetbalsimulatie", "games");
$game1->price = 40;

$game2 = new Product("Call of Duty", "First-person shooter");
$game2->price = 10;

$game3 = new Product("Minecraft", "Sandbox game", "games", 5);
$game3->price = 40;

echo $game1->getProduct();


//$game1->name = "fifa 2022";

//echo $game1->formatPrice() . "<br>";
//
//echo $game2->name . "<br>";
//echo $game2->price . "<br>";
//echo $game2->description . "<br>";
//
//echo $game3->name . "<br>";
//echo $game3->price . "<br>";
//echo $game3->description . "<br>";