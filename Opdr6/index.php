<?php

class Product
{
    public $name = "Een bepaald spel";
    public $description;
    public $price;
    public $category = "dranken"; // Toegevoegde property voor categorie met standaardwaarde "dranken"
    public $rating = 3; // Toegevoegde property voor rating met standaardwaarde 3

    public function __construct($name, $description, $category = "dranken", $rating = 3)
    {
        $this->name = strtolower($name);
        $this->description = ucfirst($description);
        $this->category = $category;
        $this->rating = $rating;
    }

    public function formatPrice()
    {
        return number_format($this->price, 0);
    }
}

$game1 = new Product(name: "fifa 2023", description: "Voetbalsimulatie", category: "games");
$game1->price = 40;

$game2 = new Product(name: "Call of Duty", description: "First-person shooter");
$game2->price = 10;

$game3 = new Product(name: "minecraft", description: "Sandbox game", rating: 5);
$game3->price = 40;

$game1->name = "fifa 2022";

echo $game1->formatPrice() . "<br>";

echo $game2->name . "<br>";
echo $game2->price . "<br>";
echo $game2->description . "<br>";

echo $game3->name . "<br>";
echo $game3->price . "<br>";
echo $game3->description . "<br>";