<?php

class Product
{
    public $name = "Een bepaald spel";

    public $price;

    public function formatPrice()
    {
        return number_format($this->price,  0);
    }
}