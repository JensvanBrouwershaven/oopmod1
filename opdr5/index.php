<?php

class Product
{
    public $name = "Een bepaald spel";
    public $description; // Nieuwe property toegevoegd
    public $price;

    public function __construct($name, $description)
    {
        $this->name = strtolower($name);
        $this->description = ucfirst($description);
    }

    public function formatPrice()
    {
        return number_format($this->price, 0);
    }
}

$game1 = new Product("fifa 2023", "Voetbalsimulatie");
$game1->price = 40;

$game2 = new Product("Call of Duty", "First-person shooter");
$game2->price = 10;

$game3 = new Product("minecraft", "Sandbox game");
$game3->price = 40;

$game1->name = "fifa 2022";

echo $game1->formatPrice() . "<br>";

echo $game2->name . "<br>";
echo $game2->price . "<br>";
echo $game2->description . "<br>"; // Beschrijving toegevoegd

echo $game3->name . "<br>";
echo $game3->price . "<br>";
echo $game3->description . "<br>"; // Beschrijving toegevoegd
